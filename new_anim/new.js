function _each(arr, fn) {
    let i = 0,
        l = arr.length;
    for (i; i < l; i++) {
        fn(i, arr[i]);
    }
}

let firstClick = document.querySelectorAll('.bullets div')
let information = document.querySelectorAll('.information div')
let point = document.querySelectorAll('.point div');

function close() {
    _each(information, function (i, el) {
        el.click();
    })
    _each(point, function (i, el) {
        el.className = '';
    });
}

firstClick[1].addEventListener('click', function () {
    close();
    firstClick[2].classList.add('linetwo')
    firstClick[3].classList.add('lineone')
    information[0].classList.remove('display_none')
    information[0].classList.add('opacityone')
})

information[2].addEventListener('click', function () {
    information[0].classList.add('display_none')
    firstClick[2].classList.remove('linetwo')
    firstClick[3].classList.remove('lineone')
    information[0].classList.remove('opacityone')
})

firstClick[4].addEventListener('click', function () {
    close()
    firstClick[6].classList.add('linethree')
    firstClick[7].classList.add('linefour')
    information[3].classList.remove('display_none')
    information[3].classList.add('opacityone')
})

information[5].addEventListener('click', function () {
    information[3].classList.add('display_none')
    firstClick[6].classList.remove('linethree')
    firstClick[7].classList.remove('linefour')
    information[3].classList.remove('opacityone')
})

firstClick[9].addEventListener('click', function () {
    close()
    firstClick[10].classList.add('linefive')
    firstClick[11].classList.add('linesix')
    information[6].classList.remove('display_none')
    information[6].classList.add('opacityone')
})

information[8].addEventListener('click', function () {
    information[6].classList.add('display_none')
    firstClick[10].classList.remove('linefive')
    firstClick[11].classList.remove('linesix')
    information[6].classList.remove('opacityone')
})


firstClick[12].addEventListener('click', function () {
    close()
    firstClick[14].classList.add('lineseven')
    // firstClick[3].classList.add('lineone')
    information[9].classList.remove('display_none')
    information[9].classList.add('opacityone')
})

information[11].addEventListener('click', function () {
    information[9].classList.add('display_none')
    firstClick[14].classList.remove('lineseven')
    // firstClick[3].classList.remove('lineone')
    information[9].classList.remove('opacityone')
})


firstClick[17].addEventListener('click', function () {
    close()
    firstClick[18].classList.add('lineeight')
    firstClick[19].classList.add('linenine')
    information[12].classList.remove('display_none')
    information[12].classList.add('opacityone')
})

information[14].addEventListener('click', function () {
    information[12].classList.add('display_none')
    firstClick[18].classList.remove('lineeight')
    firstClick[19].classList.remove('linenine')
    information[12].classList.remove('opacityone')
})

firstClick[21].addEventListener('click', function () {
    close()
    firstClick[22].classList.add('lineten')
    // firstClick[3].classList.add('lineone')
    information[15].classList.remove('display_none')
    information[15].classList.add('opacityone')
})

information[17].addEventListener('click', function () {
    information[15].classList.add('display_none')
    firstClick[22].classList.remove('lineten')
    // firstClick[3].classList.remove('lineone')
    information[15].classList.remove('opacityone')
})

firstClick[25].addEventListener('click', function () {
    close()
    firstClick[26].classList.add('linenineten')
    // firstClick[3].classList.add('lineone')
    information[18].classList.remove('display_none')
    information[18].classList.add('opacityone')
})

information[20].addEventListener('click', function () {
    information[18].classList.add('display_none')
    firstClick[26].classList.remove('linenineten')
    // firstClick[3].classList.remove('lineone')
    information[18].classList.remove('opacityone')
})

firstClick[29].addEventListener('click', function () {
    close()
    firstClick[30].classList.add('lineeightten')
    // firstClick[31].classList.add('lineone')
    information[21].classList.remove('display_none')
    information[21].classList.add('opacityone')
})

information[23].addEventListener('click', function () {
    information[21].classList.add('display_none')
    firstClick[30].classList.remove('lineeightten')
    // firstClick[31].classList.remove('lineone')
    information[21].classList.remove('opacityone')
})

firstClick[33].addEventListener('click', function () {
    close()
    firstClick[34].classList.add('lineseventen')
    // firstClick[3].classList.add('lineone')
    information[24].classList.remove('display_none')
    information[24].classList.add('opacityone')
})

information[26].addEventListener('click', function () {
    information[24].classList.add('display_none')
    firstClick[34].classList.remove('lineseventen')
    // firstClick[3].classList.remove('lineone')
    information[24].classList.remove('opacityone')
})

firstClick[37].addEventListener('click', function () {
    close()
    firstClick[38].classList.add('linesixten')
    // firstClick[3].classList.add('lineone')
    information[27].classList.remove('display_none')
    information[27].classList.add('opacityone')
})

information[29].addEventListener('click', function () {
    information[27].classList.add('display_none')
    firstClick[38].classList.remove('linesixten')
    // firstClick[3].classList.remove('lineone')
    information[27].classList.remove('opacityone')
})

firstClick[40].addEventListener('click', function () {
    close()
    firstClick[42].classList.add('linefiveten')
    // firstClick[3].classList.add('lineone')
    information[30].classList.remove('display_none')
    information[30].classList.add('opacityone')
})

information[32].addEventListener('click', function () {
    information[30].classList.add('display_none')
    firstClick[42].classList.remove('linefiveten')
    // firstClick[3].classList.remove('lineone')
    information[30].classList.remove('opacityone')
})